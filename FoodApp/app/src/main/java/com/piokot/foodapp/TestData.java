package com.piokot.foodapp;

import android.content.Context;

import com.piokot.foodapp.database.LocalSQLLiteDatabase;
import com.piokot.foodapp.model.DatabaseVersion;
import com.piokot.foodapp.model.FlavorCombination;
import com.piokot.foodapp.model.Flavoring;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.model.GeneralizedRestrict;
import com.piokot.foodapp.model.SpecifiedProduct;
import com.piokot.foodapp.model.SpecifiedRestrict;

public class TestData {


    public void addTestData(Context context) {
        LocalSQLLiteDatabase database = LocalSQLLiteDatabase.getInstance(context);

        nukeTables(database);
        addTables(database);
    }

    private void nukeTables(LocalSQLLiteDatabase database){
        database.databaseVersionDao().nukeTable();
        database.flavorCombinationDao().nukeTable();
        database.flavoringDao().nukeTable();
        database.generalizeProductDao().nukeTable();
        database.generalizeRestrictDao().nukeTable();
        database.specifiedProductDao().nukeTable();
        database.specifiedRestrictDao().nukeTable();
    }

    private void addTables(LocalSQLLiteDatabase database){
        addDatabaseVersion(database);
        addFlavoringCombinations(database);
        addFlavorings(database);
        addGeneralizedProducts(database);
        addGeneralizedRestricts(database);
        addSpecifiedProducts(database);
        addSpecifiedRestricts(database);
    }

    private void addDatabaseVersion(LocalSQLLiteDatabase database) {
        database.databaseVersionDao().add(new DatabaseVersion(0, 0));
    }

    private void addFlavoringCombinations(LocalSQLLiteDatabase database) {
        database.flavorCombinationDao().add(new FlavorCombination(1, 1, 1, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(2, 1, 2, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(3, 1, 3, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(4, 1, 4, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(5, 1, 5, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(6, 1, 6, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(7, 1, 7, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(8, 1, 8, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(9, 1, 9, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(10, 1, 10, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(11, 1, 11, 0.25));
        database.flavorCombinationDao().add(new FlavorCombination(12, 1, 12, 0.75));
        database.flavorCombinationDao().add(new FlavorCombination(13, 1, 13, 0.25));
        database.flavorCombinationDao().add(new FlavorCombination(14, 1, 14, 0.25));
        database.flavorCombinationDao().add(new FlavorCombination(15, 1, 15, 0.25));
        database.flavorCombinationDao().add(new FlavorCombination(16, 1, 16, 0));
        database.flavorCombinationDao().add(new FlavorCombination(17, 1, 17, 0));
        database.flavorCombinationDao().add(new FlavorCombination(18, 1, 18, 0));
        database.flavorCombinationDao().add(new FlavorCombination(19, 1, 19, 0));
        database.flavorCombinationDao().add(new FlavorCombination(20, 1, 20, 0));
        database.flavorCombinationDao().add(new FlavorCombination(21, 1, 21, 1));
        database.flavorCombinationDao().add(new FlavorCombination(22, 1, 22, 0.4));
        database.flavorCombinationDao().add(new FlavorCombination(23, 1, 23, 0.2));
        database.flavorCombinationDao().add(new FlavorCombination(24, 1, 24, 0.6));
        database.flavorCombinationDao().add(new FlavorCombination(25, 1, 25, 0.85));
        database.flavorCombinationDao().add(new FlavorCombination(26, 1, 26, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(27, 1, 27, 0.65));
        database.flavorCombinationDao().add(new FlavorCombination(28, 1, 28, 0.75));
        database.flavorCombinationDao().add(new FlavorCombination(29, 1, 29, 0.68));
        database.flavorCombinationDao().add(new FlavorCombination(30, 1, 30, 0));
        database.flavorCombinationDao().add(new FlavorCombination(31, 1, 31, 0.7));
        database.flavorCombinationDao().add(new FlavorCombination(32, 1, 32, 1));
        database.flavorCombinationDao().add(new FlavorCombination(33, 1, 33, 1));
        database.flavorCombinationDao().add(new FlavorCombination(34, 1, 34, 1));
        database.flavorCombinationDao().add(new FlavorCombination(35, 1, 35, 0.62));
        database.flavorCombinationDao().add(new FlavorCombination(36, 2, 1, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(37, 2, 2, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(38, 2, 3, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(39, 2, 4, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(40, 2, 5, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(41, 2, 6, 0.35));
        database.flavorCombinationDao().add(new FlavorCombination(42, 2, 7, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(43, 2, 8, 0.2));
        database.flavorCombinationDao().add(new FlavorCombination(44, 2, 9, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(45, 2, 10, 0.615));
        database.flavorCombinationDao().add(new FlavorCombination(46, 2, 11, 0));
        database.flavorCombinationDao().add(new FlavorCombination(47, 2, 12, 0));
        database.flavorCombinationDao().add(new FlavorCombination(48, 2, 13, 0));
        database.flavorCombinationDao().add(new FlavorCombination(49, 2, 14, 0));
        database.flavorCombinationDao().add(new FlavorCombination(50, 2, 15, 0));
        database.flavorCombinationDao().add(new FlavorCombination(51, 2, 16, 0));
        database.flavorCombinationDao().add(new FlavorCombination(52, 2, 17, 0));
        database.flavorCombinationDao().add(new FlavorCombination(53, 2, 18, 0));
        database.flavorCombinationDao().add(new FlavorCombination(54, 2, 19, 0));
        database.flavorCombinationDao().add(new FlavorCombination(55, 2, 20, 0));
        database.flavorCombinationDao().add(new FlavorCombination(56, 2, 21, 0.75));
        database.flavorCombinationDao().add(new FlavorCombination(57, 2, 22, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(58, 2, 23, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(59, 2, 24, 0.167));
        database.flavorCombinationDao().add(new FlavorCombination(60, 2, 25, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(61, 2, 26, 0));
        database.flavorCombinationDao().add(new FlavorCombination(62, 2, 27, 0.22));
        database.flavorCombinationDao().add(new FlavorCombination(63, 2, 28, 0.1));
        database.flavorCombinationDao().add(new FlavorCombination(64, 2, 29, 0.18));
        database.flavorCombinationDao().add(new FlavorCombination(65, 2, 30, 0));
        database.flavorCombinationDao().add(new FlavorCombination(66, 2, 31, 0));
        database.flavorCombinationDao().add(new FlavorCombination(67, 2, 32, 0));
        database.flavorCombinationDao().add(new FlavorCombination(68, 2, 33, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(69, 2, 34, 0.312));
        database.flavorCombinationDao().add(new FlavorCombination(70, 2, 35, 0.05));
        database.flavorCombinationDao().add(new FlavorCombination(71, 3, 1, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(72, 3, 2, 0.42));
        database.flavorCombinationDao().add(new FlavorCombination(73, 3, 3, 0.123));
        database.flavorCombinationDao().add(new FlavorCombination(74, 3, 4, 1));
        database.flavorCombinationDao().add(new FlavorCombination(75, 3, 5, 0.695));
        database.flavorCombinationDao().add(new FlavorCombination(76, 3, 6, 0.132));
        database.flavorCombinationDao().add(new FlavorCombination(77, 3, 7, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(78, 3, 8, 0.623));
        database.flavorCombinationDao().add(new FlavorCombination(79, 3, 9, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(80, 3, 10, 0.378));
        database.flavorCombinationDao().add(new FlavorCombination(81, 3, 11, 0));
        database.flavorCombinationDao().add(new FlavorCombination(82, 3, 12, 0));
        database.flavorCombinationDao().add(new FlavorCombination(83, 3, 13, 0));
        database.flavorCombinationDao().add(new FlavorCombination(84, 3, 14, 0));
        database.flavorCombinationDao().add(new FlavorCombination(85, 3, 15, 0));
        database.flavorCombinationDao().add(new FlavorCombination(86, 3, 16, 0));
        database.flavorCombinationDao().add(new FlavorCombination(87, 3, 17, 0));
        database.flavorCombinationDao().add(new FlavorCombination(88, 3, 18, 0));
        database.flavorCombinationDao().add(new FlavorCombination(89, 3, 19, 0));
        database.flavorCombinationDao().add(new FlavorCombination(90, 3, 20, 0));
        database.flavorCombinationDao().add(new FlavorCombination(91, 3, 21, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(92, 3, 22, 0.2));
        database.flavorCombinationDao().add(new FlavorCombination(93, 3, 23, 0));
        database.flavorCombinationDao().add(new FlavorCombination(94, 3, 24, 0.45));
        database.flavorCombinationDao().add(new FlavorCombination(95, 3, 25, 0.25));
        database.flavorCombinationDao().add(new FlavorCombination(96, 3, 26, 0));
        database.flavorCombinationDao().add(new FlavorCombination(97, 3, 27, 0));
        database.flavorCombinationDao().add(new FlavorCombination(98, 3, 28, 0.15));
        database.flavorCombinationDao().add(new FlavorCombination(99, 3, 29, 0.34));
        database.flavorCombinationDao().add(new FlavorCombination(100, 3, 30, 0.1));
        database.flavorCombinationDao().add(new FlavorCombination(101, 3, 31, 0));
        database.flavorCombinationDao().add(new FlavorCombination(102, 3, 32, 0.19));
        database.flavorCombinationDao().add(new FlavorCombination(103, 3, 33, 0.3));
        database.flavorCombinationDao().add(new FlavorCombination(104, 3, 34, 0.2));
        database.flavorCombinationDao().add(new FlavorCombination(105, 3, 35, 0.05));
        database.flavorCombinationDao().add(new FlavorCombination(106, 4, 1, 0.85));
        database.flavorCombinationDao().add(new FlavorCombination(107, 4, 2, 0.72));
        database.flavorCombinationDao().add(new FlavorCombination(108, 4, 3, 0.9));
        database.flavorCombinationDao().add(new FlavorCombination(109, 4, 4, 0.66));
        database.flavorCombinationDao().add(new FlavorCombination(110, 4, 5, 0.9));
        database.flavorCombinationDao().add(new FlavorCombination(111, 4, 6, 0.4));
        database.flavorCombinationDao().add(new FlavorCombination(112, 4, 7, 0.2));
        database.flavorCombinationDao().add(new FlavorCombination(113, 4, 8, 0));
        database.flavorCombinationDao().add(new FlavorCombination(114, 4, 9, 0));
        database.flavorCombinationDao().add(new FlavorCombination(115, 4, 10, 0.9));
        database.flavorCombinationDao().add(new FlavorCombination(116, 4, 11, 0));
        database.flavorCombinationDao().add(new FlavorCombination(117, 4, 12, 0.85));
        database.flavorCombinationDao().add(new FlavorCombination(118, 4, 13, 0.65));
        database.flavorCombinationDao().add(new FlavorCombination(119, 4, 14, 0));
        database.flavorCombinationDao().add(new FlavorCombination(120, 4, 15, 0));
        database.flavorCombinationDao().add(new FlavorCombination(121, 4, 16, 1));
        database.flavorCombinationDao().add(new FlavorCombination(122, 4, 17, 1));
        database.flavorCombinationDao().add(new FlavorCombination(123, 4, 18, 1));
        database.flavorCombinationDao().add(new FlavorCombination(124, 4, 19, 1));
        database.flavorCombinationDao().add(new FlavorCombination(125, 4, 20, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(126, 4, 21, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(127, 4, 22, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(128, 4, 23, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(129, 4, 24, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(130, 4, 25, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(131, 4, 26, 0.22));
        database.flavorCombinationDao().add(new FlavorCombination(132, 4, 27, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(133, 4, 28, 0.1));
        database.flavorCombinationDao().add(new FlavorCombination(134, 4, 29, 0.6));
        database.flavorCombinationDao().add(new FlavorCombination(135, 4, 30, 0.6));
        database.flavorCombinationDao().add(new FlavorCombination(136, 4, 31, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(137, 4, 32, 0.1));
        database.flavorCombinationDao().add(new FlavorCombination(138, 4, 33, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(139, 4, 34, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(140, 4, 35, 0.6));
        database.flavorCombinationDao().add(new FlavorCombination(141, 5, 1, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(142, 5, 2, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(143, 5, 3, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(144, 5, 4, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(145, 5, 5, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(146, 5, 6, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(147, 5, 7, 0.8));
        database.flavorCombinationDao().add(new FlavorCombination(148, 5, 8, 0.8));
        database.flavorCombinationDao().add(new FlavorCombination(149, 5, 9, 0.35));
        database.flavorCombinationDao().add(new FlavorCombination(150, 5, 10, 1));
        database.flavorCombinationDao().add(new FlavorCombination(151, 5, 11, 1));
        database.flavorCombinationDao().add(new FlavorCombination(152, 5, 12, 1));
        database.flavorCombinationDao().add(new FlavorCombination(153, 5, 13, 1));
        database.flavorCombinationDao().add(new FlavorCombination(154, 5, 14, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(155, 5, 15, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(156, 5, 16, 0));
        database.flavorCombinationDao().add(new FlavorCombination(157, 5, 17, 0));
        database.flavorCombinationDao().add(new FlavorCombination(158, 5, 18, 0));
        database.flavorCombinationDao().add(new FlavorCombination(159, 5, 19, 0));
        database.flavorCombinationDao().add(new FlavorCombination(160, 5, 20, 0));
        database.flavorCombinationDao().add(new FlavorCombination(161, 5, 21, 0.87));
        database.flavorCombinationDao().add(new FlavorCombination(162, 5, 22, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(163, 5, 23, 0.23));
        database.flavorCombinationDao().add(new FlavorCombination(164, 5, 24, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(165, 5, 25, 0.8));
        database.flavorCombinationDao().add(new FlavorCombination(166, 5, 26, 1));
        database.flavorCombinationDao().add(new FlavorCombination(167, 5, 27, 0));
        database.flavorCombinationDao().add(new FlavorCombination(168, 5, 28, 1));
        database.flavorCombinationDao().add(new FlavorCombination(169, 5, 29, 0.8));
        database.flavorCombinationDao().add(new FlavorCombination(170, 5, 30, 0.643));
        database.flavorCombinationDao().add(new FlavorCombination(171, 5, 31, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(172, 5, 32, 0.285));
        database.flavorCombinationDao().add(new FlavorCombination(173, 5, 33, 0.5));
        database.flavorCombinationDao().add(new FlavorCombination(174, 5, 34, 0.4));
        database.flavorCombinationDao().add(new FlavorCombination(175, 5, 35, 0));    }

    private void addFlavorings(LocalSQLLiteDatabase database) {
        database.flavoringDao().add(new Flavoring(1, "bazylia"));
        database.flavoringDao().add(new Flavoring(2, "chili"));
        database.flavoringDao().add(new Flavoring(3, "kminek"));
        database.flavoringDao().add(new Flavoring(4, "sezam"));
        database.flavoringDao().add(new Flavoring(5, "ziola prowansalskie"));
    }

    private void addGeneralizedProducts(LocalSQLLiteDatabase database) {
        database.generalizeProductDao().add(new GeneralizedProduct(1, "pieczywo"));
        database.generalizeProductDao().add(new GeneralizedProduct(2, "mieso"));
        database.generalizeProductDao().add(new GeneralizedProduct(3, "ryby"));
        database.generalizeProductDao().add(new GeneralizedProduct(4, "owoce"));
        database.generalizeProductDao().add(new GeneralizedProduct(5, "warzywa"));
        database.generalizeProductDao().add(new GeneralizedProduct(6, "przetwory mleczne"));
        database.generalizeProductDao().add(new GeneralizedProduct(7, "nasiona"));
    }

    private void addGeneralizedRestricts(LocalSQLLiteDatabase database) {
        database.generalizeRestrictDao().add(new GeneralizedRestrict(1, 1, 1));
        database.generalizeRestrictDao().add(new GeneralizedRestrict(2, 2, 3));
        database.generalizeRestrictDao().add(new GeneralizedRestrict(3, 3, 3));
    }

    private void addSpecifiedProducts(LocalSQLLiteDatabase database) {
        database.specifiedProductDao().add(new SpecifiedProduct(1, 1, "chleb pszenny"));
        database.specifiedProductDao().add(new SpecifiedProduct(2, 1, "bulka graham"));
        database.specifiedProductDao().add(new SpecifiedProduct(3, 1, "rogalik"));
        database.specifiedProductDao().add(new SpecifiedProduct(4, 1, "chleb zytni"));
        database.specifiedProductDao().add(new SpecifiedProduct(5, 1, "chleb razowy"));
        database.specifiedProductDao().add(new SpecifiedProduct(6, 2, "salami"));
        database.specifiedProductDao().add(new SpecifiedProduct(7, 2, "krakowska sucha"));
        database.specifiedProductDao().add(new SpecifiedProduct(8, 2, "szynka z pieca"));
        database.specifiedProductDao().add(new SpecifiedProduct(9, 2, "kindziuk"));
        database.specifiedProductDao().add(new SpecifiedProduct(10, 2, "boczek"));
        database.specifiedProductDao().add(new SpecifiedProduct(11, 3, "sledz"));
        database.specifiedProductDao().add(new SpecifiedProduct(12, 3, "losos wedzony"));
        database.specifiedProductDao().add(new SpecifiedProduct(13, 3, "szprotki"));
        database.specifiedProductDao().add(new SpecifiedProduct(14, 3, "karmazyn wedzony"));
        database.specifiedProductDao().add(new SpecifiedProduct(15, 3, "makrela wedzona"));
        database.specifiedProductDao().add(new SpecifiedProduct(16, 4, "zurawina"));
        database.specifiedProductDao().add(new SpecifiedProduct(17, 4, "dzem owocowy"));
        database.specifiedProductDao().add(new SpecifiedProduct(18, 4, "maliny"));
        database.specifiedProductDao().add(new SpecifiedProduct(19, 4, "jagody"));
        database.specifiedProductDao().add(new SpecifiedProduct(20, 4, "wiorki kokosowe"));
        database.specifiedProductDao().add(new SpecifiedProduct(21, 5, "cebula"));
        database.specifiedProductDao().add(new SpecifiedProduct(22, 5, "szczypiorek"));
        database.specifiedProductDao().add(new SpecifiedProduct(23, 5, "rukola"));
        database.specifiedProductDao().add(new SpecifiedProduct(24, 5, "papryka czerwona"));
        database.specifiedProductDao().add(new SpecifiedProduct(25, 5, "ogorek zielony"));
        database.specifiedProductDao().add(new SpecifiedProduct(26, 5, "kapary"));
        database.specifiedProductDao().add(new SpecifiedProduct(27, 5, "kukurydza"));
        database.specifiedProductDao().add(new SpecifiedProduct(28, 6, "feta"));
        database.specifiedProductDao().add(new SpecifiedProduct(29, 6, "gouda"));
        database.specifiedProductDao().add(new SpecifiedProduct(30, 6, "serek topiony"));
        database.specifiedProductDao().add(new SpecifiedProduct(31, 6, "twarog"));
        database.specifiedProductDao().add(new SpecifiedProduct(32, 6, "ser plesniowy"));
        database.specifiedProductDao().add(new SpecifiedProduct(33, 7, "slonecznik"));
        database.specifiedProductDao().add(new SpecifiedProduct(34, 7, "nasiona dyni"));
        database.specifiedProductDao().add(new SpecifiedProduct(35, 7, "pestki granatu"));    }

    private void addSpecifiedRestricts(LocalSQLLiteDatabase database) {
        database.specifiedRestrictDao().add(new SpecifiedRestrict(1, 11, 17));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(2, 11, 28));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(3, 11, 30));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(4, 11, 32));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(5, 13, 16));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(6, 13, 17));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(7, 13, 18));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(8, 13, 19));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(9, 13, 20));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(10, 13, 31));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(11, 13, 32));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(12, 14, 16));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(13, 14, 17));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(14, 14, 18));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(15, 14, 19));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(16, 14, 20));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(17, 14, 28));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(18, 14, 29));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(19, 14, 30));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(20, 14, 31));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(21, 14, 32));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(22, 15, 16));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(23, 15, 17));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(24, 15, 18));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(25, 15, 19));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(26, 15, 20));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(27, 15, 28));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(28, 15, 29));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(29, 15, 30));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(30, 15, 31));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(31, 15, 32));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(32, 16, 26));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(33, 17, 26));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(34, 18, 26));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(35, 19, 26));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(36, 20, 26));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(37, 28, 31));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(38, 28, 32));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(39, 29, 32));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(40, 30, 32));
        database.specifiedRestrictDao().add(new SpecifiedRestrict(41, 31, 32));    }
}
