package com.piokot.foodapp.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.piokot.foodapp.R;
import com.piokot.foodapp.form.ProductAddPreferencesActivityForm;
import com.piokot.foodapp.service.BannedProductsService;
import com.piokot.foodapp.service.GeneralizeProductService;
import com.piokot.foodapp.service.SpecifiedProductService;
import com.piokot.foodapp.service.impl.GeneralizeProductServiceImpl;
import com.piokot.foodapp.service.impl.SpecifiedProductServiceImpl;
import com.piokot.foodapp.singletonwrapper.BannedProducts;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SuppressLint("Registered")
public class AddPreferencesActivity extends AppCompatActivity {

    private final SpecifiedProductService specifiedProductService = new SpecifiedProductServiceImpl();
    private final GeneralizeProductService generalizeProductService = new GeneralizeProductServiceImpl();
    private final BannedProductsService bannedProductsService = BannedProducts.getInstance();
    private final List<CheckBox> productsCheckboxes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_preference);

        Bundle b = getIntent().getExtras();
        int value = -1; // or other values
        if (b != null) {
            value = b.getInt("key");
            TextView textView = findViewById(R.id.settings_title);
            textView.setText("Settings for " + generalizeProductService.getCategoryNameById(value).toUpperCase());
            addProductsCheckboxes(value);
        }

        Objects.requireNonNull(getSupportActionBar()).hide();
    }

    private void addProductsCheckboxes(int value) {
        List<ProductAddPreferencesActivityForm> products = specifiedProductService.getProductsBySpecifiedCategoryId(value);

        LinearLayout.LayoutParams layoutParams;
        LinearLayout linearLayout = findViewById(R.id.products_checkboxes);

        for (ProductAddPreferencesActivityForm product : products) {
            layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            layoutParams.setMargins(5, 5, 5, 5);
            LinearLayout coordinatorLayout = new LinearLayout(this);
            coordinatorLayout.setLayoutParams(layoutParams);
            coordinatorLayout.setOrientation(LinearLayout.HORIZONTAL);
            coordinatorLayout.setGravity(Gravity.LEFT);
//            coordinatorLayout.setBackground();

            layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView textView = new TextView(this);
            textView.setLayoutParams(layoutParams);
            textView.setText(product.getName().toUpperCase());
            textView.setTextColor(Color.BLACK);
            textView.setTextSize(25);

            layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            CheckBox checkBox = new CheckBox(this);
            checkBox.setLayoutParams(layoutParams);
            checkBox.setId(product.getId());
            checkBox.setChecked(!bannedProductsService.checkIfExistBannedProduct(product.getId()));
            productsCheckboxes.add(checkBox);

            coordinatorLayout.addView(textView);
            coordinatorLayout.addView(checkBox);
            linearLayout.addView(coordinatorLayout);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent myIntent = new Intent(AddPreferencesActivity.this, PreferencesActivity.class);
            startActivity(myIntent);
            finish();

            return true;
        }

        return super.onKeyDown(keyCode, keyEvent);
    }


    public void saveProducts(View view) {
        for (CheckBox checkBox : productsCheckboxes) {
            if (checkBox.isChecked()) {
                bannedProductsService.removeBannedProduct(checkBox.getId());
            } else {
                bannedProductsService.addBannedProduct(checkBox.getId());
            }
        }
    }

}
