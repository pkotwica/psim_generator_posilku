package com.piokot.foodapp.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.piokot.foodapp.R;
import com.piokot.foodapp.TestData;
import com.piokot.foodapp.database.LocalSQLLiteDatabase;
import com.piokot.foodapp.database.RemoteDatabaseServer;

@SuppressLint("Registered")
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Bundle b = getIntent().getExtras();
        int value = -1; // or other values
        if (b != null) {
            value = b.getInt("key");
        }

        LocalSQLLiteDatabase.getInstance(getApplicationContext());
        if(value != 1) {
            //remote database
            RemoteDatabaseServer remoteDatabaseServer = new RemoteDatabaseServer();
            boolean isSuccess = remoteDatabaseServer.updateLocalDatabase(getApplicationContext());
//
            if (!isSuccess) {
                displayPopup();
            }

            //local test database
//            TestData testData = new TestData();
//            testData.addTestData(getApplicationContext());
        }

        setContentView(R.layout.activity_main);
    }

    @SuppressLint("WrongConstant")
    private void displayPopup() {
        Intent a = new Intent(getApplicationContext(), DatabasePopupActivity.class);
        a.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        startActivity(a);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void generateMeal(View view) {
        Intent myIntent = new Intent(view.getContext(), MealActivity.class);
        startActivity(myIntent);
    }

    public void settings(View view) {
        Intent myIntent = new Intent(view.getContext(), PreferencesActivity.class);
        startActivity(myIntent);
    }
}
