package com.piokot.foodapp.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.piokot.foodapp.R;
import com.piokot.foodapp.form.RandomFlavoringMealActivityForm;
import com.piokot.foodapp.form.RandomProductMealActivityForm;
import com.piokot.foodapp.service.FoodGeneratorService;
import com.piokot.foodapp.service.impl.FoodGeneratorServiceImpl;

import java.util.List;
import java.util.Objects;

@SuppressLint("Registered")
public class MealActivity extends AppCompatActivity {

    private final FoodGeneratorService foodGeneratorService = new FoodGeneratorServiceImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal);

        List<RandomProductMealActivityForm> products = foodGeneratorService.generateMeal();
        List<RandomFlavoringMealActivityForm> flavorings = foodGeneratorService.selectFlavoring(products);
        addGeneratedProducts(products);
        addSelectedFlavorings(flavorings);

        Objects.requireNonNull(getSupportActionBar()).hide();
    }

    @SuppressLint("SetTextI18n")
    private void addSelectedFlavorings(List<RandomFlavoringMealActivityForm> flavorings) {
        LinearLayout flavoringsLayout = findViewById(R.id.random_flavorings);
        for (RandomFlavoringMealActivityForm flavoring : flavorings) {
            LinearLayout.LayoutParams layoutParamsWrapWrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView textView = new TextView(this);
            textView.setLayoutParams(layoutParamsWrapWrap);
            textView.setText((flavorings.indexOf(flavoring)+1) + ": " + flavoring.getFlavoringName());
            textView.setTextColor(Color.rgb(1, 1, 1));
            textView.setTextSize(30);

            flavoringsLayout.addView(textView);
        }
    }

    private void addGeneratedProducts(List<RandomProductMealActivityForm> products) {
        LinearLayout productsLayout = findViewById(R.id.random_products);
        for (RandomProductMealActivityForm product : products) {
            LinearLayout.LayoutParams layoutParamsWrapWrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView textView = new TextView(this);
            textView.setLayoutParams(layoutParamsWrapWrap);
            textView.setText(product.getCategoryName() + ":\t" + product.getProductName());
            textView.setTextColor(Color.rgb(1, 1, 1));
            textView.setTextSize(30);

            productsLayout.addView(textView);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent myIntent = new Intent(MealActivity.this, MainActivity.class);
            Bundle b = new Bundle();
            b.putInt("key", 1); //Your id
            myIntent.putExtras(b);
            startActivity(myIntent);
            finish();

            return true;
        }

        return super.onKeyDown(keyCode, keyEvent);
    }

    public void generateAgain(View view) {
        Intent myIntent = new Intent(MealActivity.this, MealActivity.class);
        startActivity(myIntent);
        finish();
    }

}
