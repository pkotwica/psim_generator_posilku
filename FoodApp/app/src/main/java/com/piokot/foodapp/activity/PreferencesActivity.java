package com.piokot.foodapp.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewManager;
import android.view.ViewParent;
import android.view.WindowId;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.piokot.foodapp.R;
import com.piokot.foodapp.form.BannedProductMainActivityForm;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.service.BannedProductsService;
import com.piokot.foodapp.service.GeneralizeProductService;
import com.piokot.foodapp.service.SpecifiedProductService;
import com.piokot.foodapp.service.impl.GeneralizeProductServiceImpl;
import com.piokot.foodapp.service.impl.SpecifiedProductServiceImpl;
import com.piokot.foodapp.singletonwrapper.BannedProducts;

import java.util.ArrayList;
import java.util.List;

public class PreferencesActivity extends Activity {

    private final int LARGE_MEAL = 3;
    private final int MEDIUM_MEAL = 2;
    private final int SMALL_MEAL = 1;

    private final GeneralizeProductService generalizeProductService = new GeneralizeProductServiceImpl();
    private final SpecifiedProductService specifiedProductService = new SpecifiedProductServiceImpl();
    private final BannedProductsService bannedProductsService = BannedProducts.getInstance();
    private final List<FloatingActionButton> productRemoveButtons = new ArrayList<>();


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        addButtonsForCategories();

        addBannedProductsFields();

        setTouchedButtonOfSizeMeal();
    }

    private void setTouchedButtonOfSizeMeal() {
        Button buttonSmall = findViewById(R.id.size_small);
        Button buttonMedium = findViewById(R.id.size_medium);
        Button buttonLarge = findViewById(R.id.size_large);

        switch(BannedProducts.getInstance().getSizeOfMeal()){
            case SMALL_MEAL:
                buttonSmall.setBackgroundResource(R.drawable.button_touched);
                buttonMedium.setBackgroundResource(R.drawable.button_shape);
                buttonLarge.setBackgroundResource(R.drawable.button_shape);
                break;
            case MEDIUM_MEAL:
                buttonSmall.setBackgroundResource(R.drawable.button_shape);
                buttonMedium.setBackgroundResource(R.drawable.button_touched);
                buttonLarge.setBackgroundResource(R.drawable.button_shape);
                break;
            case LARGE_MEAL:
                buttonSmall.setBackgroundResource(R.drawable.button_shape);
                buttonMedium.setBackgroundResource(R.drawable.button_shape);
                buttonLarge.setBackgroundResource(R.drawable.button_touched);
                break;
        }
    }

    @SuppressLint("RtlHardcoded")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void addBannedProductsFields() {
        List<BannedProductMainActivityForm> bannedProducts = specifiedProductService.getAllBannedProductsNamesWithCategoryName();

        LinearLayout.LayoutParams layoutParamsMatchWrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams layoutParamsWrapWrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout bannedProductsLayout = findViewById(R.id.banned_products);
        LinearLayout linearLayout;
        LinearLayout wrapperLinearLayout;
        TextView textViewCategory;
        TextView textViewProduct;
        FloatingActionButton floatingActionButton;

        for (BannedProductMainActivityForm bannedProduct : bannedProducts) {
            linearLayout = new LinearLayout(this);
            linearLayout.setLayoutParams(layoutParamsMatchWrap);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);

            wrapperLinearLayout = new LinearLayout(this);
            wrapperLinearLayout.setLayoutParams(layoutParamsMatchWrap);
            wrapperLinearLayout.setGravity(Gravity.LEFT);
            wrapperLinearLayout.setBackground(Drawable.createFromPath("#CBCBCB"));
            wrapperLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
            wrapperLinearLayout.setPadding(30, 30, 30, 30);

            textViewCategory = new TextView(this);
            layoutParamsWrapWrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            textViewCategory.setLayoutParams(layoutParamsWrapWrap);
            textViewCategory.setGravity(Gravity.LEFT);
            textViewCategory.setText(bannedProduct.getCategoryName().toUpperCase() + ": ");
            textViewCategory.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewCategory.setTextColor(Color.BLACK);
            textViewCategory.setTextSize(20);

            textViewProduct = new TextView(this);
            layoutParamsWrapWrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            textViewProduct.setLayoutParams(layoutParamsWrapWrap);
            textViewProduct.setGravity(Gravity.LEFT);
            textViewProduct.setText(bannedProduct.getSpecifiedProductName().toLowerCase());
            textViewProduct.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            textViewProduct.setTextColor(Color.BLACK);
            textViewProduct.setTextSize(20);

            floatingActionButton = new FloatingActionButton(this);
//            layoutParamsWrapWrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.anchorGravity = Gravity.RIGHT;
            floatingActionButton.setLayoutParams(layoutParams);
            floatingActionButton.setId(bannedProduct.getSpecifiedProductId());
            floatingActionButton.setScaleX((float) 0.5);
            floatingActionButton.setScaleY((float) 0.5);
            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorAccent)));
            floatingActionButton.setImageResource(R.drawable.icon_x);
            final FloatingActionButton finalFloatingActionButton = floatingActionButton;
            floatingActionButton.setOnClickListener(view -> {
                ViewParent parent = finalFloatingActionButton.getParent();
                ((ViewManager) parent.getParent()).removeView((View) parent);
                bannedProductsService.removeBannedProduct(finalFloatingActionButton.getId());
            });
            productRemoveButtons.add(floatingActionButton);

            wrapperLinearLayout.addView(textViewCategory);
            wrapperLinearLayout.addView(textViewProduct);
            linearLayout.addView(wrapperLinearLayout);
            linearLayout.addView(floatingActionButton);
            bannedProductsLayout.addView(linearLayout);
        }

    }

    @SuppressLint("ResourceAsColor")
    private void addButtonsForCategories() {
        List<GeneralizedProduct> generalizedProducts = generalizeProductService.getAllGeneralizedProducts();

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        layoutParams.setMargins(R.dimen.button_margin, R.dimen.button_margin, R.dimen.button_margin, R.dimen.button_margin);
        layoutParams.setMargins(10, 10, 10, 10);
        LinearLayout linearLayout = findViewById(R.id.categories_buttons);
        Button button;

        for (final GeneralizedProduct generalizedProduct : generalizedProducts) {
//            button = new Button(new ContextThemeWrapper(this, R.style.Button_Preference), null, 0);
            button = new Button(this);
            button.setLayoutParams(layoutParams);
            button.setId(generalizedProduct.getId());
            button.setText(generalizedProduct.getFoodType());
            button.setBackgroundResource(R.drawable.button_shape);
            button.setTextSize(25);
            button.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
            button.setOnClickListener(view -> {
                Intent intent = new Intent(view.getContext(), AddPreferencesActivity.class);
                Bundle b = new Bundle();
                b.putInt("key", generalizedProduct.getId()); //Your id
                intent.putExtras(b); //Put your id to your next Intent
                startActivity(intent);
                finish();
            });

            linearLayout.addView(button);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent myIntent = new Intent(PreferencesActivity.this, MainActivity.class);
            Bundle b = new Bundle();
            b.putInt("key", 1); //Your id
            myIntent.putExtras(b);
            startActivity(myIntent);
            finish();

            return true;
        }

        return super.onKeyDown(keyCode, keyEvent);
    }

    public void setSizeOfMeal(View view) {
        switch(view.getId()){
            case R.id.size_small:
                BannedProducts.getInstance().setSizeOfMeal(SMALL_MEAL);
                break;
            case R.id.size_medium:
                BannedProducts.getInstance().setSizeOfMeal(MEDIUM_MEAL);
                break;
            case R.id.size_large:
                BannedProducts.getInstance().setSizeOfMeal(LARGE_MEAL);
                break;
        }

        setTouchedButtonOfSizeMeal();
    }
}
