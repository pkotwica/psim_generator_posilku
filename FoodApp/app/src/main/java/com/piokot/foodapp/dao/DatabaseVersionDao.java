package com.piokot.foodapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.piokot.foodapp.model.DatabaseVersion;

@Dao
public interface DatabaseVersionDao {
    @Query("SELECT * FROM database_version where id LIKE  :id")
    DatabaseVersion findById(int id);

    @Insert
    void add(DatabaseVersion databaseVersion);

    @Update
    void update(DatabaseVersion databaseVersion);

    @Query("DELETE FROM database_version")
    void nukeTable();
}
