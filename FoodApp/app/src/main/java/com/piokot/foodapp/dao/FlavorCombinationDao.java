package com.piokot.foodapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.piokot.foodapp.model.FlavorCombination;

import java.util.List;

@Dao
public interface FlavorCombinationDao {
    @Query("SELECT * FROM flavor_combinations")
    List<FlavorCombination> getAll();

    @Query("SELECT * FROM flavor_combinations where id LIKE  :id")
    FlavorCombination findById(int id);

    @Insert
    void add(FlavorCombination flavorCombination);

    @Query("DELETE FROM flavor_combinations")
    void nukeTable();

    @Query("SELECT * FROM flavor_combinations WHERE flavoring_id LIKE :flavoringId")
    List<FlavorCombination> findByFlavoringId(int flavoringId);
}
