package com.piokot.foodapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.piokot.foodapp.model.Flavoring;

import java.util.List;

@Dao
public interface FlavoringDao {
    @Query("SELECT * FROM flavorings")
    List<Flavoring> getAll();

    @Query("SELECT * FROM flavorings where id LIKE  :id")
    Flavoring findById(int id);

    @Insert
    void add(Flavoring flavoring);

    @Query("DELETE FROM flavorings")
    void nukeTable();
}
