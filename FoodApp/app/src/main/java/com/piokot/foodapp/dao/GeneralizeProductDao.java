package com.piokot.foodapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.piokot.foodapp.model.GeneralizedProduct;

import java.util.List;

@Dao
public interface GeneralizeProductDao {
    @Query("SELECT * FROM generalized_products")
    List<GeneralizedProduct> getAll();

    @Query("SELECT * FROM generalized_products where id LIKE  :id")
    GeneralizedProduct findById(int id);

    @Insert
    void add(GeneralizedProduct generalizedProduct);

    @Query("DELETE FROM generalized_products")
    void nukeTable();
}
