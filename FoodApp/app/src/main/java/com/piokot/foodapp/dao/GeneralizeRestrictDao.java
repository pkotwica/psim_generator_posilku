package com.piokot.foodapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.piokot.foodapp.model.GeneralizedRestrict;

import java.util.List;

@Dao
public interface GeneralizeRestrictDao {
    @Query("SELECT * FROM generalized_restricts")
    List<GeneralizedRestrict> getAll();

    @Query("SELECT * FROM generalized_restricts where id LIKE  :id")
    GeneralizedRestrict findById(int id);

    @Insert
    void add(GeneralizedRestrict generalizedRestrict);

    @Query("DELETE FROM generalized_restricts")
    void nukeTable();
}
