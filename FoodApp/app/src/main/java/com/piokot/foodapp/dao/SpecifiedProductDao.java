package com.piokot.foodapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.piokot.foodapp.model.SpecifiedProduct;

import java.util.List;

@Dao
public interface SpecifiedProductDao {
    @Query("SELECT * FROM specified_products")
    List<SpecifiedProduct> getAll();

    @Query("SELECT * FROM specified_products where id LIKE  :id")
    SpecifiedProduct findById(int id);

    @Insert
    void add(SpecifiedProduct specifiedProduct);

    @Query("DELETE FROM specified_products")
    void nukeTable();

    @Query("SELECT COUNT(*) FROM specified_products")
    int getAmountOfProducts();
}
