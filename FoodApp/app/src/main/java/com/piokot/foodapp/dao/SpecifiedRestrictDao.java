package com.piokot.foodapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.piokot.foodapp.model.SpecifiedRestrict;

import java.util.List;

@Dao
public interface SpecifiedRestrictDao {
    @Query("SELECT * FROM specified_restricts")
    List<SpecifiedRestrict> getAll();

    @Query("SELECT * FROM specified_restricts where id LIKE  :id")
    SpecifiedRestrict findById(int id);

    @Insert
    void add(SpecifiedRestrict specifiedRestrict);

    @Query("DELETE FROM specified_restricts")
    void nukeTable();
}
