package com.piokot.foodapp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.piokot.foodapp.dao.DatabaseVersionDao;
import com.piokot.foodapp.dao.FlavorCombinationDao;
import com.piokot.foodapp.dao.FlavoringDao;
import com.piokot.foodapp.dao.GeneralizeProductDao;
import com.piokot.foodapp.dao.GeneralizeRestrictDao;
import com.piokot.foodapp.dao.SpecifiedProductDao;
import com.piokot.foodapp.dao.SpecifiedRestrictDao;
import com.piokot.foodapp.model.DatabaseVersion;
import com.piokot.foodapp.model.FlavorCombination;
import com.piokot.foodapp.model.Flavoring;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.model.GeneralizedRestrict;
import com.piokot.foodapp.model.SpecifiedProduct;
import com.piokot.foodapp.model.SpecifiedRestrict;

@Database(entities = {
        DatabaseVersion.class,
        FlavorCombination.class,
        Flavoring.class,
        GeneralizedProduct.class,
        GeneralizedRestrict.class,
        SpecifiedProduct.class,
        SpecifiedRestrict.class},
        version = 1,
        exportSchema = false)
public abstract class LocalSQLLiteDatabase extends RoomDatabase {

    //singleton
    private static LocalSQLLiteDatabase INSTANCE;

    public abstract DatabaseVersionDao databaseVersionDao();
    public abstract FlavorCombinationDao flavorCombinationDao();
    public abstract FlavoringDao flavoringDao();
    public abstract GeneralizeProductDao generalizeProductDao();
    public abstract GeneralizeRestrictDao generalizeRestrictDao();
    public abstract SpecifiedRestrictDao specifiedRestrictDao();
    public abstract SpecifiedProductDao specifiedProductDao();

    public static LocalSQLLiteDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), LocalSQLLiteDatabase.class, "food-app-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static LocalSQLLiteDatabase getInstance(){
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
