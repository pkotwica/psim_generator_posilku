package com.piokot.foodapp.database;

import android.content.Context;

import com.piokot.foodapp.database.parser.FoodAppJSONParser;
import com.piokot.foodapp.model.DatabaseVersion;
import com.piokot.foodapp.model.FlavorCombination;
import com.piokot.foodapp.model.Flavoring;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.model.GeneralizedRestrict;
import com.piokot.foodapp.model.SpecifiedProduct;
import com.piokot.foodapp.model.SpecifiedRestrict;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class RemoteDatabaseServer {

    private static final String DATABASE_SERVICE_URL = "http://chomik.ict.pwr.wroc.pl:5000";

    private static final String GENERALIZED_PRODUCTS_TABLE = "generalized_products";
    private static final String SPECIFIED_PRODUCTS_TABLE = "specified_products";
    private static final String GENERALIZED_RESTRICTS_TABLE = "generalized_restricts";
    private static final String SPECIFIED_RESTRICTS_TABLE = "specified_restricts";
    private static final String FLAVORINGS_TABLE = "flavorings";
    private static final String FLAVOR_COMBINATIONS_TABLE = "flavor_combinations";
    private static final String DATABASE_VERSION = "database_version";


    public boolean updateLocalDatabase(Context context) {
        LocalSQLLiteDatabase database = LocalSQLLiteDatabase.getInstance(context);
        int localVersionOfDatabase = -1;
        Boolean isSuccess = Boolean.TRUE;
        int remoteVersionOfDatabase = 0;

        try {
            remoteVersionOfDatabase = getActualVersionOfRemoteDatabase();
        } catch (IOException e) {
            e.printStackTrace();
            isSuccess = Boolean.FALSE;
        } catch (JSONException e) {
            e.printStackTrace();
            isSuccess = Boolean.FALSE;
        }

        DatabaseVersion databaseVersion = database.databaseVersionDao().findById(0);
        if (databaseVersion != null) {
            localVersionOfDatabase = databaseVersion.getVersion();
        }

        if ((localVersionOfDatabase == -1 || localVersionOfDatabase < remoteVersionOfDatabase) && isSuccess) {
            clearAllTables(database);

            loadAllDataToTablesFromRemotesDatabase(database);

            updateLocalVersionOfDatabase(remoteVersionOfDatabase, database);
        }

        return isSuccess;
    }

    private void loadAllDataToTablesFromRemotesDatabase(LocalSQLLiteDatabase database) {
        updateGeneralizedProducts(database);
        updateSpecifiedProducts(database);
        updateGeneralizedRestricts(database);
        updateSpecifiedRestricts(database);
        updateFlavorings(database);
        updateFlavorCombinations(database);
    }

    private void updateLocalVersionOfDatabase(int version, LocalSQLLiteDatabase database) {
        database.databaseVersionDao().update(new DatabaseVersion(0, version));
    }

    private void clearAllTables(LocalSQLLiteDatabase database) {
        database.flavorCombinationDao().nukeTable();
        database.flavoringDao().nukeTable();
        database.generalizeProductDao().nukeTable();
        database.generalizeRestrictDao().nukeTable();
        database.specifiedProductDao().nukeTable();
        database.specifiedRestrictDao().nukeTable();
    }


    private JSONObject getTable(String nameOfTable) throws JSONException, IOException {
        String str = DATABASE_SERVICE_URL + "/" + nameOfTable;
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        URL url = new URL(str);
        urlConn = url.openConnection();
        bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

        StringBuilder stringBuffer = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuffer.append(line);
        }

        return new JSONObject(stringBuffer.toString());
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    private void updateGeneralizedProducts(LocalSQLLiteDatabase database) {
        JSONObject generalizedProductsJSON = new JSONObject();
        List<GeneralizedProduct> generalizedProducts = new ArrayList<>();

        try {
            generalizedProductsJSON = getTable(GENERALIZED_PRODUCTS_TABLE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            generalizedProducts = FoodAppJSONParser.parseGeneralizedProducts(generalizedProductsJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (GeneralizedProduct generalizedProduct : generalizedProducts) {
            database.generalizeProductDao().add(generalizedProduct);
        }
    }

    private void updateSpecifiedProducts(LocalSQLLiteDatabase database) {
        JSONObject specifiedProductsJSON = new JSONObject();
        List<SpecifiedProduct> specifiedProducts = new ArrayList<>();

        try {
            specifiedProductsJSON = getTable(SPECIFIED_PRODUCTS_TABLE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            specifiedProducts = FoodAppJSONParser.parseSpecifiedProducts(specifiedProductsJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (SpecifiedProduct specifiedProduct : specifiedProducts) {
            database.specifiedProductDao().add(specifiedProduct);
        }
    }

    private void updateGeneralizedRestricts(LocalSQLLiteDatabase database) {
        JSONObject generalizedRestrictsJSON = new JSONObject();
        List<GeneralizedRestrict> generalizedRestricts = new ArrayList<>();

        try {
            generalizedRestrictsJSON = getTable(GENERALIZED_RESTRICTS_TABLE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            generalizedRestricts = FoodAppJSONParser.parseGeneralizedRestricts(generalizedRestrictsJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (GeneralizedRestrict generalizedRestrict : generalizedRestricts) {
            database.generalizeRestrictDao().add(generalizedRestrict);
        }
    }

    private void updateSpecifiedRestricts(LocalSQLLiteDatabase database) {
        JSONObject specifiedRestrictsJSON = new JSONObject();
        List<SpecifiedRestrict> specifiedRestricts = new ArrayList<>();

        try {
            specifiedRestrictsJSON = getTable(SPECIFIED_RESTRICTS_TABLE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            specifiedRestricts = FoodAppJSONParser.parseSpecifiedRestricts(specifiedRestrictsJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (SpecifiedRestrict specifiedRestrict : specifiedRestricts) {
            database.specifiedRestrictDao().add(specifiedRestrict);
        }
    }

    private void updateFlavorings(LocalSQLLiteDatabase database) {
        JSONObject flavoringsJSON = new JSONObject();
        List<Flavoring> flavorings = new ArrayList<>();

        try {
            flavoringsJSON = getTable(FLAVORINGS_TABLE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            flavorings = FoodAppJSONParser.parseFlavorings(flavoringsJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (Flavoring flavoring : flavorings) {
            database.flavoringDao().add(flavoring);
        }
    }

    private void updateFlavorCombinations(LocalSQLLiteDatabase database) {
        JSONObject flavorCombinationsJSON = new JSONObject();
        List<FlavorCombination> flavorCombinations = new ArrayList<>();

        try {
            flavorCombinationsJSON = getTable(FLAVOR_COMBINATIONS_TABLE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            flavorCombinations = FoodAppJSONParser.parseFlavorCombinations(flavorCombinationsJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (FlavorCombination flavorCombination : flavorCombinations) {
            database.flavorCombinationDao().add(flavorCombination);
        }
    }

    private int getActualVersionOfRemoteDatabase() throws IOException, JSONException {
        JSONObject databaseVersionJSON = getTable(DATABASE_VERSION);

        return FoodAppJSONParser.parseDatabaseVersion(databaseVersionJSON);
    }
}
