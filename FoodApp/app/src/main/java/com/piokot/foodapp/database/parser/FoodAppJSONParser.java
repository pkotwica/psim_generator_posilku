package com.piokot.foodapp.database.parser;

import com.piokot.foodapp.model.FlavorCombination;
import com.piokot.foodapp.model.Flavoring;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.model.GeneralizedRestrict;
import com.piokot.foodapp.model.SpecifiedProduct;
import com.piokot.foodapp.model.SpecifiedRestrict;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Class has static functions for parsing JSON object containing one list called by database table name.
 * The list contains all objects with fields called by equivalent of database column.
 */
public class FoodAppJSONParser {

    public static List<GeneralizedProduct> parseGeneralizedProducts(JSONObject JSONGeneralizedProducts) throws JSONException {
        List<GeneralizedProduct> generalizedProducts = new ArrayList<>();
        JSONArray jsonArray = JSONGeneralizedProducts.getJSONArray("generalized_products");
        int id;
        String foodType;

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                id = Integer.parseInt(jsonArray.getJSONObject(i).getString("general_product_id"));
                foodType = jsonArray.getJSONObject(i).getString("food_type");
                generalizedProducts.add(new GeneralizedProduct(id, foodType));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return generalizedProducts;
    }

    public static List<SpecifiedProduct> parseSpecifiedProducts(JSONObject JSONSpecifiedProducts) throws JSONException {
        List<SpecifiedProduct> specifiedProducts = new ArrayList<>();
        JSONArray jsonArray = JSONSpecifiedProducts.getJSONArray("specified_products");
        int id;
        int generalizedProductId;
        String foodName;

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                id = Integer.parseInt(jsonArray.getJSONObject(i).getString("specific_product_id"));
                generalizedProductId = Integer.parseInt(jsonArray.getJSONObject(i).getString("general_product_id"));
                foodName = jsonArray.getJSONObject(i).getString("food_name");
                specifiedProducts.add(new SpecifiedProduct(id, generalizedProductId, foodName));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return specifiedProducts;
    }

    public static List<GeneralizedRestrict> parseGeneralizedRestricts(JSONObject JSONGeneralizedRestricts) throws JSONException {
        List<GeneralizedRestrict> generalizedRestricts = new ArrayList<>();
        JSONArray jsonArray = JSONGeneralizedRestricts.getJSONArray("generalized_restricts");
        int id;
        int firstGeneralProductId;
        int secondGeneralProductId;

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                id = Integer.parseInt(jsonArray.getJSONObject(i).getString("general_restrict_id"));
                firstGeneralProductId = Integer.parseInt(jsonArray.getJSONObject(i).getString("first_general_product_id"));
                secondGeneralProductId = Integer.parseInt(jsonArray.getJSONObject(i).getString("second_general_product_id"));
                generalizedRestricts.add(new GeneralizedRestrict(id, firstGeneralProductId, secondGeneralProductId));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return generalizedRestricts;
    }

    public static List<SpecifiedRestrict> parseSpecifiedRestricts(JSONObject JSONSpecifiedRestricts) throws JSONException {
        List<SpecifiedRestrict> specifiedRestricts = new ArrayList<>();
        JSONArray jsonArray = JSONSpecifiedRestricts.getJSONArray("specified_restricts");
        int id;
        int firstSpecificProductId;
        int secondSpecificProductId;

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                id = Integer.parseInt(jsonArray.getJSONObject(i).getString("specific_restrict_id"));
                firstSpecificProductId = Integer.parseInt(jsonArray.getJSONObject(i).getString("first_specific_product_id"));
                secondSpecificProductId = Integer.parseInt(jsonArray.getJSONObject(i).getString("second_specific_product_id"));
                specifiedRestricts.add(new SpecifiedRestrict(id, firstSpecificProductId, secondSpecificProductId));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return specifiedRestricts;
    }

    public static List<Flavoring> parseFlavorings(JSONObject JSONFlavorings) throws JSONException {
        List<Flavoring> flavorings = new ArrayList<>();
        JSONArray jsonArray = JSONFlavorings.getJSONArray("flavorings");
        int id;
        String flavoringName;

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                id = Integer.parseInt(jsonArray.getJSONObject(i).getString("flavoring_id"));
                flavoringName = jsonArray.getJSONObject(i).getString("flavoring_name");
                flavorings.add(new Flavoring(id, flavoringName));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return flavorings;
    }

    public static List<FlavorCombination> parseFlavorCombinations(JSONObject JSONFlavorCombinations) throws JSONException {
        List<FlavorCombination> flavorCombinations = new ArrayList<>();
        JSONArray jsonArray = JSONFlavorCombinations.getJSONArray("flavor_combinations");
        int id;
        int flavoringId;
        int specificProductId;
        double fitLevel;

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                id = Integer.parseInt(jsonArray.getJSONObject(i).getString("flavor_combination_id"));
                flavoringId = Integer.parseInt(jsonArray.getJSONObject(i).getString("flavoring_id"));
                specificProductId = Integer.parseInt(jsonArray.getJSONObject(i).getString("specific_product_id"));
                fitLevel = Double.parseDouble(jsonArray.getJSONObject(i).getString("fit_level"));
                flavorCombinations.add(new FlavorCombination(id, flavoringId, specificProductId, fitLevel));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return flavorCombinations;
    }

    public static int parseDatabaseVersion(JSONObject JSONDatabaseVersion) {
        int version = 0;

        try {
            version = Integer.parseInt(JSONDatabaseVersion.getString("version"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return version;
    }
}
