package com.piokot.foodapp.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BannedProductMainActivityForm {
    int specifiedProductId;
    String specifiedProductName;
    String categoryName;
}
