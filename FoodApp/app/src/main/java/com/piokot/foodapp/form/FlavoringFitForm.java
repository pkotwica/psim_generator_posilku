package com.piokot.foodapp.form;

import com.piokot.foodapp.model.Flavoring;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FlavoringFitForm {
    Flavoring flavoring;
    double fit;
}
