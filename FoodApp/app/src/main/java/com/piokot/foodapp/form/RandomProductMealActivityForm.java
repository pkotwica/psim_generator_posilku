package com.piokot.foodapp.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RandomProductMealActivityForm {
    private String categoryName;
    private String productName;
    private int productId;
}
