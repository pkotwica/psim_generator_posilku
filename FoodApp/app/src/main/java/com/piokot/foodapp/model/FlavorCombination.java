package com.piokot.foodapp.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(tableName = "flavor_combinations")
public class FlavorCombination {
    @PrimaryKey
    int id;
    @ColumnInfo(name = "flavoring_id")
    int flavoringId;
    @ColumnInfo(name = "specific_product_id")
    int specificProductId;
    @ColumnInfo(name = "fit_level")
    double fitLevel;
}
