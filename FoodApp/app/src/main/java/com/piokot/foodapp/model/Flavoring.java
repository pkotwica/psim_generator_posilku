package com.piokot.foodapp.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(tableName = "flavorings")
public class Flavoring {
    @PrimaryKey
    int id;
    @ColumnInfo(name = "flavoring_name")
    String flavoringName;
}
