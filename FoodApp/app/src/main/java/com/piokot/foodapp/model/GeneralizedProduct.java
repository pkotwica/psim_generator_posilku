package com.piokot.foodapp.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(tableName = "generalized_products")
public class GeneralizedProduct {
    @PrimaryKey
    int id;
    @ColumnInfo(name = "food_type")
    String foodType;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof GeneralizedProduct)) return false;
        GeneralizedProduct o = (GeneralizedProduct) obj;
        return o.getId() == this.getId();
    }
}
