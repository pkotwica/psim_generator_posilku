package com.piokot.foodapp.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(tableName = "generalized_restricts")
public class GeneralizedRestrict {
    @PrimaryKey
    int id;
    @ColumnInfo(name = "first_general_product_id")
    int firstGeneralProductId;
    @ColumnInfo(name = "second_general_product_id")
    int secondGeneralProductId;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof GeneralizedRestrict)) return false;
        GeneralizedRestrict o = (GeneralizedRestrict) obj;
        return o.getId() == this.getId();
    }
}
