package com.piokot.foodapp.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(tableName = "specified_products")
public class SpecifiedProduct {
    @PrimaryKey
    int id;
    @ColumnInfo(name = "general_product_id")
    int generalizedProductId;
    @ColumnInfo(name = "food_name")
    String foodName;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof SpecifiedProduct)) return false;
        SpecifiedProduct o = (SpecifiedProduct) obj;
        return o.getId() == this.getId();
    }
}
