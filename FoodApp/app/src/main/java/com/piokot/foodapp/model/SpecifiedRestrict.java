package com.piokot.foodapp.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(tableName = "specified_restricts")
public class SpecifiedRestrict {
    @PrimaryKey
    int id;
    @ColumnInfo(name = "first_specific_product_id")
    int firstSpecificProductId;
    @ColumnInfo(name = "second_specific_product_id")
    int secondSpecificProductId;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof SpecifiedRestrict)) return false;
        SpecifiedRestrict o = (SpecifiedRestrict) obj;
        return o.getId() == this.getId();
    }
}
