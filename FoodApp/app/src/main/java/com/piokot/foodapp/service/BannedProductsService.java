package com.piokot.foodapp.service;

import java.util.List;

public interface BannedProductsService {

    void addBannedProduct(int idOfSpecifiedProduct);

    void removeBannedProduct(int idOfSpecifiedProduct);

    List<Integer> getAllBannedProduct();

    boolean checkIfExistBannedProduct(int idOfSpecifiedProduct);

    int getSizeOfMeal();

    void setSizeOfMeal(int size);
}
