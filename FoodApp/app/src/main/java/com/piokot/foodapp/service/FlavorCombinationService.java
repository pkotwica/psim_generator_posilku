package com.piokot.foodapp.service;

import java.util.List;

public interface FlavorCombinationService {

    double calculateFitForSpecifiedProductAndFlavoring(List<Integer> products, int flavoringId);
}
