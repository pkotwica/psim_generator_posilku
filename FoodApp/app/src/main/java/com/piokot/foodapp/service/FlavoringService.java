package com.piokot.foodapp.service;

import com.piokot.foodapp.form.FlavoringFitForm;

import java.util.List;

public interface FlavoringService {

    List<FlavoringFitForm> searchFlavoringsFitsForProducts(List<Integer> products);
}
