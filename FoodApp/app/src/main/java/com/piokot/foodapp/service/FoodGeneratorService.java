package com.piokot.foodapp.service;

import com.piokot.foodapp.form.RandomFlavoringMealActivityForm;
import com.piokot.foodapp.form.RandomProductMealActivityForm;

import java.util.List;

public interface FoodGeneratorService {

    List<RandomProductMealActivityForm> generateMeal();

    List<RandomFlavoringMealActivityForm> selectFlavoring(List<RandomProductMealActivityForm> products);
}
