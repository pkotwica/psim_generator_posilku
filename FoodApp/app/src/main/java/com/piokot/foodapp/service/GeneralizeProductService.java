package com.piokot.foodapp.service;

import com.piokot.foodapp.model.GeneralizedProduct;

import java.util.List;

public interface GeneralizeProductService {

    List<GeneralizedProduct> getAllGeneralizedProducts();

    String getCategoryNameById(int categoryId);
}
