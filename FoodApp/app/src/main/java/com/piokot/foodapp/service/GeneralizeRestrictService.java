package com.piokot.foodapp.service;

import com.piokot.foodapp.model.GeneralizedProduct;

public interface GeneralizeRestrictService {

    boolean isRestrictBetweenTypes(GeneralizedProduct firstGeneralizedProduct, GeneralizedProduct secondGeneralizedProduct);

    boolean isRestrictBetweenTypes(int firstGeneralizedProduct, int secondGeneralizedProduct);
}
