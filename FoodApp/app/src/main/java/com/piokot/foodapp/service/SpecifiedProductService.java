package com.piokot.foodapp.service;

import com.piokot.foodapp.form.BannedProductMainActivityForm;
import com.piokot.foodapp.form.ProductAddPreferencesActivityForm;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.model.SpecifiedProduct;

import java.util.List;

public interface SpecifiedProductService {

    List<BannedProductMainActivityForm> getAllBannedProductsNamesWithCategoryName();

    String getCategoryNameByProductId(int specifiedProductId);

    List<ProductAddPreferencesActivityForm> getProductsBySpecifiedCategoryId(int categoryId);

    List<SpecifiedProduct> getProductsBySpecifiedCategoryId(GeneralizedProduct generalizedProduct);

    int getAmountOfProducts();
}
