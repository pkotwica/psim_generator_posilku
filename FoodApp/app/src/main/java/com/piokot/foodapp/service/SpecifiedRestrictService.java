package com.piokot.foodapp.service;

import com.piokot.foodapp.model.SpecifiedProduct;

public interface SpecifiedRestrictService {

    boolean isRestrictBetweenProducts(SpecifiedProduct firstSpecifiedProduct, SpecifiedProduct secondSpecifiedProduct);

    boolean isRestrictBetweenProducts(int firstSpecifiedProduct, int secondSpecifiedProduct);
}
