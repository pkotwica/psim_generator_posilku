package com.piokot.foodapp.service.impl;

import com.piokot.foodapp.service.BannedProductsService;

import java.util.ArrayList;
import java.util.List;

public class BannedProductsServiceImpl implements BannedProductsService {
    private List<Integer> listOfBannedProducts = new ArrayList<>();
    private int amountOfMeal = 1;

//    private BannedProductsServiceImpl() {}

    @Override
    public void addBannedProduct(int idOfSpecifiedProduct) {
        if (!listOfBannedProducts.contains(idOfSpecifiedProduct)) {
            listOfBannedProducts.add(idOfSpecifiedProduct);
        }
    }

    @Override
    public void removeBannedProduct(int idOfSpecifiedProduct) {
        if (listOfBannedProducts.contains(idOfSpecifiedProduct)) {
            listOfBannedProducts.remove(Integer.valueOf(idOfSpecifiedProduct));
        }
    }

    @Override
    public List<Integer> getAllBannedProduct() {
        return new ArrayList<>(listOfBannedProducts);
    }

    @Override
    public boolean checkIfExistBannedProduct(int idOfSpecifiedProduct) {
        return listOfBannedProducts.contains(idOfSpecifiedProduct);
    }

    @Override
    public int getSizeOfMeal() {
        return amountOfMeal;
    }

    @Override
    public void setSizeOfMeal(int size) {
        this.amountOfMeal = size;
    }
}
