package com.piokot.foodapp.service.impl;

import com.piokot.foodapp.database.LocalSQLLiteDatabase;
import com.piokot.foodapp.model.FlavorCombination;
import com.piokot.foodapp.service.FlavorCombinationService;

import java.util.List;

public class FlavorCombinationServiceImpl implements FlavorCombinationService {
    private final LocalSQLLiteDatabase database = LocalSQLLiteDatabase.getInstance();

    @Override
    public double calculateFitForSpecifiedProductAndFlavoring(List<Integer> products, int flavoringId) {
        List<FlavorCombination> flavorCombinations = database.flavorCombinationDao().findByFlavoringId(flavoringId);
        double averageFit = 0;

        for (FlavorCombination flavorCombination : flavorCombinations) {
            if (products.contains(flavorCombination.getSpecificProductId())) {
                averageFit += flavorCombination.getFitLevel();
            }
        }

        return averageFit / products.size();
    }
}
