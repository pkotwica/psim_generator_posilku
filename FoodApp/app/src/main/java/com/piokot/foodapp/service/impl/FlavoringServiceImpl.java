package com.piokot.foodapp.service.impl;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.piokot.foodapp.database.LocalSQLLiteDatabase;
import com.piokot.foodapp.form.FlavoringFitForm;
import com.piokot.foodapp.model.Flavoring;
import com.piokot.foodapp.service.FlavorCombinationService;
import com.piokot.foodapp.service.FlavoringService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FlavoringServiceImpl implements FlavoringService {
    private final LocalSQLLiteDatabase database = LocalSQLLiteDatabase.getInstance();
    private final FlavorCombinationService flavorCombinationService = new FlavorCombinationServiceImpl();

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public List<FlavoringFitForm> searchFlavoringsFitsForProducts(List<Integer> productsIds) {
        List<Flavoring> flavorings = database.flavoringDao().getAll();
        List<FlavoringFitForm> flavoringFitForms = new ArrayList<>();

        for (Flavoring flavoring : flavorings) {
            flavoringFitForms.add(new FlavoringFitForm(flavoring, flavorCombinationService.calculateFitForSpecifiedProductAndFlavoring(productsIds, flavoring.getId())));
        }

        flavoringFitForms.sort(Comparator.comparing(FlavoringFitForm::getFit).reversed());
        return flavoringFitForms;
    }
}
