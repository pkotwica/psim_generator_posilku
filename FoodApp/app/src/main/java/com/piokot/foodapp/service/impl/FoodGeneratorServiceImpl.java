package com.piokot.foodapp.service.impl;

import com.piokot.foodapp.database.LocalSQLLiteDatabase;
import com.piokot.foodapp.form.FlavoringFitForm;
import com.piokot.foodapp.form.RandomFlavoringMealActivityForm;
import com.piokot.foodapp.form.RandomProductMealActivityForm;
import com.piokot.foodapp.model.Flavoring;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.model.GeneralizedRestrict;
import com.piokot.foodapp.model.SpecifiedProduct;
import com.piokot.foodapp.service.BannedProductsService;
import com.piokot.foodapp.service.FlavoringService;
import com.piokot.foodapp.service.FoodGeneratorService;
import com.piokot.foodapp.service.GeneralizeRestrictService;
import com.piokot.foodapp.service.SpecifiedProductService;
import com.piokot.foodapp.singletonwrapper.BannedProducts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class FoodGeneratorServiceImpl implements FoodGeneratorService {

    private final int LIMIT_PRODUCTS_SMALL = 2;   //amount of products to generate(besides of pieczywo)
    private final int LIMIT_PRODUCTS_MEDIUM = 3;   //amount of products to generate(besides of pieczywo)
    private final int LIMIT_PRODUCTS_LARGE = 4;   //amount of products to generate(besides of pieczywo)
    private final int LIMIT_FLAVORINGS = 3;   //amount of flavorings to generate

    private final SpecifiedProductService specifiedProductService = new SpecifiedProductServiceImpl();
    private final LocalSQLLiteDatabase database = LocalSQLLiteDatabase.getInstance();
    private final BannedProductsService bannedProductsService = BannedProducts.getInstance();
    private final SpecifiedRestrictServiceImpl specifiedRestrictService = new SpecifiedRestrictServiceImpl();
    private final GeneralizeRestrictService generalizeRestrictService = new GeneralizeRestrictServiceImpl();
    private final FlavoringService flavoringService = new FlavoringServiceImpl();

    private int sizeOfMeal = LIMIT_PRODUCTS_SMALL;

    @Override
    public List<RandomProductMealActivityForm> generateMeal() {
        int amountOfProducts = specifiedProductService.getAmountOfProducts();
        List<GeneralizedProduct> buildRandomTypes;

        setSizeOfMeal();

        if(amountOfProducts > 0) {
            buildRandomTypes = randBuildList();
            return translateProductList(specifyProducts(buildRandomTypes));
        }

        return new ArrayList<>();
    }

    private void setSizeOfMeal() {
        switch(BannedProducts.getInstance().getSizeOfMeal()){
            case 1:
                sizeOfMeal = LIMIT_PRODUCTS_SMALL;
                break;
            case 2:
                sizeOfMeal = LIMIT_PRODUCTS_MEDIUM;
                break;
            case 3:
                sizeOfMeal = LIMIT_PRODUCTS_LARGE;
                break;
        }
    }

    private List<GeneralizedProduct> randBuildList() {
        List<GeneralizedProduct> buildRandomTypes = new ArrayList<>();
        List<GeneralizedProduct> openRandomTypes = database.generalizeProductDao().getAll();
        GeneralizedProduct pieczywo = database.generalizeProductDao().findById(1);
        buildRandomTypes.add(pieczywo);
        openRandomTypes.remove(pieczywo);

        for (int i = 0; i < sizeOfMeal; i++) {
            GeneralizedProduct randGeneralizedProduct;

            randGeneralizedProduct = openRandomTypes.get(randInt(0, openRandomTypes.size() - 1));

            buildRandomTypes.add(randGeneralizedProduct);

            refreshOpenList(openRandomTypes, randGeneralizedProduct);
        }

        return buildRandomTypes;
    }

    private void refreshOpenList(List<GeneralizedProduct> openRandomTypes, GeneralizedProduct randGeneralizedProduct) {
        List<GeneralizedRestrict> generalizedRestricts = database.generalizeRestrictDao().getAll();
        List<Integer> idsOfGeneralizedProductsToDelete = new ArrayList<>();

        for (GeneralizedRestrict generalizedRestrict : generalizedRestricts) {
            if (generalizedRestrict.getFirstGeneralProductId() == randGeneralizedProduct.getId()) {
                idsOfGeneralizedProductsToDelete.add(generalizedRestrict.getSecondGeneralProductId());
            }

            if (generalizedRestrict.getSecondGeneralProductId() == randGeneralizedProduct.getId()) {
                idsOfGeneralizedProductsToDelete.add(generalizedRestrict.getFirstGeneralProductId());
            }
        }

        for (Integer generalProductId : idsOfGeneralizedProductsToDelete) {
            GeneralizedProduct generalizedProductToDelete = database.generalizeProductDao().findById(generalProductId);

            openRandomTypes.remove(generalizedProductToDelete);
        }
    }

    private List<SpecifiedProduct> specifyProducts(List<GeneralizedProduct> buildRandomTypes) {
        List<SpecifiedProduct> randomProducts = new ArrayList<>();

        for (GeneralizedProduct generalizedProduct : buildRandomTypes) {
            List<SpecifiedProduct> proposedSpecifiedProducts = specifiedProductService.getProductsBySpecifiedCategoryId(generalizedProduct);

            rejectProductsByBanned(proposedSpecifiedProducts);

            rejectProductsByRestricts(randomProducts, proposedSpecifiedProducts);

            if (proposedSpecifiedProducts.size() > 0) {
                randomProducts.add(proposedSpecifiedProducts.get(randInt(0, proposedSpecifiedProducts.size() - 1)));
            }
        }

        return randomProducts;
    }

    private void rejectProductsByBanned(List<SpecifiedProduct> proposedSpecifiedProducts) {
        Iterator productIterator = proposedSpecifiedProducts.iterator();
        while (productIterator.hasNext()) {
            SpecifiedProduct currentSpecifiedProduct = (SpecifiedProduct) productIterator.next();
            if (bannedProductsService.checkIfExistBannedProduct(currentSpecifiedProduct.getId())) {
                productIterator.remove();
            }
        }
    }

    private void rejectProductsByRestricts(List<SpecifiedProduct> randomProducts, List<SpecifiedProduct> proposedSpecifiedProducts) {
        for (SpecifiedProduct specifiedProduct : randomProducts) {
            Iterator productIterator = proposedSpecifiedProducts.iterator();
            while (productIterator.hasNext()) {
                SpecifiedProduct proposedProduct = (SpecifiedProduct) productIterator.next();
                if (specifiedRestrictService.isRestrictBetweenProducts(specifiedProduct, proposedProduct) || randomProducts.contains(proposedProduct)) {
                    productIterator.remove();
                }
            }
        }
    }

    private List<RandomProductMealActivityForm> translateProductList(List<SpecifiedProduct> products) {
        List<RandomProductMealActivityForm> translatedList = new ArrayList<>();

        for (SpecifiedProduct product : products) {
            translatedList.add(new RandomProductMealActivityForm(
                            specifiedProductService.getCategoryNameByProductId(product.getId()),
                            product.getFoodName(),
                            product.getId()
                    )
            );
        }

        return translatedList;
    }

    @Override
    public List<RandomFlavoringMealActivityForm> selectFlavoring(List<RandomProductMealActivityForm> products) {
        if(products.size() > 0) {
            List<Flavoring> fittedFlavorings = new ArrayList<>();
            List<Integer> productsIds = new ArrayList<>();

            for (RandomProductMealActivityForm product : products) {
                productsIds.add(product.getProductId());
            }

            List<FlavoringFitForm> flavoringFits = flavoringService.searchFlavoringsFitsForProducts(productsIds);

            for (int i = 0; i < LIMIT_FLAVORINGS; i++) {
                if (flavoringFits.get(i).getFit() > 0) {
                    fittedFlavorings.add(flavoringFits.get(i).getFlavoring());
                }
            }

            return translateFlavoringList(fittedFlavorings);
        }

        return new ArrayList<>();
    }

    private List<RandomFlavoringMealActivityForm> translateFlavoringList(List<Flavoring> flavorings) {
        List<RandomFlavoringMealActivityForm> translatedList = new ArrayList<>();

        for (Flavoring flavoring : flavorings) {
            translatedList.add(new RandomFlavoringMealActivityForm(flavoring.getFlavoringName()));
        }

        return translatedList;
    }

    private int randInt(int minimum, int maximum) {
        return ThreadLocalRandom.current().nextInt(minimum, maximum + 1);
    }
}
