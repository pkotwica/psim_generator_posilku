package com.piokot.foodapp.service.impl;

import com.piokot.foodapp.database.LocalSQLLiteDatabase;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.service.GeneralizeProductService;

import java.util.List;

public class GeneralizeProductServiceImpl implements GeneralizeProductService {

    private final LocalSQLLiteDatabase database = LocalSQLLiteDatabase.getInstance();

    @Override
    public List<GeneralizedProduct> getAllGeneralizedProducts() {
        return database.generalizeProductDao().getAll();
    }

    @Override
    public String getCategoryNameById(int categoryId) {
        GeneralizedProduct generalizedProduct = database.generalizeProductDao().findById(categoryId);
        return generalizedProduct.getFoodType();
    }
}
