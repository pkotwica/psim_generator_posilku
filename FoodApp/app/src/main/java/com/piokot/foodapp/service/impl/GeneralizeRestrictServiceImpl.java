package com.piokot.foodapp.service.impl;

import com.piokot.foodapp.database.LocalSQLLiteDatabase;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.model.GeneralizedRestrict;
import com.piokot.foodapp.model.SpecifiedProduct;
import com.piokot.foodapp.model.SpecifiedRestrict;
import com.piokot.foodapp.service.GeneralizeRestrictService;

import java.util.List;

public class GeneralizeRestrictServiceImpl implements GeneralizeRestrictService {
    private final LocalSQLLiteDatabase database = LocalSQLLiteDatabase.getInstance();

    @Override
    public boolean isRestrictBetweenTypes(GeneralizedProduct firstGeneralizedProduct, GeneralizedProduct secondGeneralizedProduct) {
        List<GeneralizedRestrict> generalizedRestricts = database.generalizeRestrictDao().getAll();

        for (GeneralizedRestrict generalizedRestrict : generalizedRestricts) {
            if (generalizedRestrict.getFirstGeneralProductId() == firstGeneralizedProduct.getId() && generalizedRestrict.getSecondGeneralProductId() == secondGeneralizedProduct.getId()) {
                return true;
            }

            if (generalizedRestrict.getFirstGeneralProductId() == secondGeneralizedProduct.getId() && generalizedRestrict.getSecondGeneralProductId() == firstGeneralizedProduct.getId()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isRestrictBetweenTypes(int firstGeneralizedProduct, int secondGeneralizedProduct) {
        List<GeneralizedRestrict> generalizedRestricts = database.generalizeRestrictDao().getAll();

        for (GeneralizedRestrict generalizedRestrict : generalizedRestricts) {
            if (generalizedRestrict.getFirstGeneralProductId() == firstGeneralizedProduct && generalizedRestrict.getSecondGeneralProductId() == secondGeneralizedProduct) {
                return true;
            }

            if (generalizedRestrict.getFirstGeneralProductId() == secondGeneralizedProduct && generalizedRestrict.getSecondGeneralProductId() == firstGeneralizedProduct) {
                return true;
            }
        }

        return false;
    }
}
