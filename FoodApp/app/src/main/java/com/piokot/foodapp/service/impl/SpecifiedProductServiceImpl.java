package com.piokot.foodapp.service.impl;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.piokot.foodapp.database.LocalSQLLiteDatabase;
import com.piokot.foodapp.form.BannedProductMainActivityForm;
import com.piokot.foodapp.form.ProductAddPreferencesActivityForm;
import com.piokot.foodapp.model.GeneralizedProduct;
import com.piokot.foodapp.model.SpecifiedProduct;
import com.piokot.foodapp.service.BannedProductsService;
import com.piokot.foodapp.service.GeneralizeProductService;
import com.piokot.foodapp.service.SpecifiedProductService;
import com.piokot.foodapp.singletonwrapper.BannedProducts;

import java.util.ArrayList;
import java.util.List;

public class SpecifiedProductServiceImpl implements SpecifiedProductService {

    private final LocalSQLLiteDatabase database = LocalSQLLiteDatabase.getInstance();
    private BannedProductsService bannedProductsService = BannedProducts.getInstance();
    private GeneralizeProductService generalizeProductService = new GeneralizeProductServiceImpl();

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public List<BannedProductMainActivityForm> getAllBannedProductsNamesWithCategoryName() {
        List<BannedProductMainActivityForm> productsByCategories = new ArrayList<>();

        List<Integer> allBannedProduct = bannedProductsService.getAllBannedProduct();

        for (Integer productId : allBannedProduct) {
            BannedProductMainActivityForm form = new BannedProductMainActivityForm(productId, getNameOfProductByProductId(productId), getCategoryNameByProductId(productId));
            productsByCategories.add(form);
        }

        //TODO: it would be ok if list will be sorted

        return productsByCategories;
    }

    @Override
    public String getCategoryNameByProductId(int specifiedProductId) {
        SpecifiedProduct specifiedProduct = database.specifiedProductDao().findById(specifiedProductId);
        return generalizeProductService.getCategoryNameById(specifiedProduct.getGeneralizedProductId());
    }

    @Override
    public List<ProductAddPreferencesActivityForm> getProductsBySpecifiedCategoryId(int categoryId) {
        List<SpecifiedProduct> specifiedProducts = database.specifiedProductDao().getAll();
        List<ProductAddPreferencesActivityForm> specifiedCategoryProducts = new ArrayList<>();

        for (SpecifiedProduct specifiedProduct : specifiedProducts) {
            if (specifiedProduct.getGeneralizedProductId() == categoryId) {
                specifiedCategoryProducts.add(new ProductAddPreferencesActivityForm(specifiedProduct.getId(), specifiedProduct.getFoodName()));
            }
        }

        return specifiedCategoryProducts;
    }

    @Override
    public List<SpecifiedProduct> getProductsBySpecifiedCategoryId(GeneralizedProduct generalizedProduct) {
        List<SpecifiedProduct> specifiedProducts = database.specifiedProductDao().getAll();
        List<SpecifiedProduct> specifiedProductsByCategory = new ArrayList<>();

        for (SpecifiedProduct specifiedProduct : specifiedProducts) {
            if (specifiedProduct.getGeneralizedProductId() == generalizedProduct.getId()) {
                specifiedProductsByCategory.add(specifiedProduct);
            }
        }

        return specifiedProductsByCategory;
    }

    @Override
    public int getAmountOfProducts() {
        return database.specifiedProductDao().getAmountOfProducts();
    }

    private String getNameOfProductByProductId(Integer productId) {
        SpecifiedProduct specifiedProduct = database.specifiedProductDao().findById(productId);
        return specifiedProduct.getFoodName();
    }

}
