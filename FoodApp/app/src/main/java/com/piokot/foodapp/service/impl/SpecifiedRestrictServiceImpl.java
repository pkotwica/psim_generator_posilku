package com.piokot.foodapp.service.impl;

import com.piokot.foodapp.database.LocalSQLLiteDatabase;
import com.piokot.foodapp.model.SpecifiedProduct;
import com.piokot.foodapp.model.SpecifiedRestrict;
import com.piokot.foodapp.service.SpecifiedRestrictService;

import java.util.List;

public class SpecifiedRestrictServiceImpl implements SpecifiedRestrictService {
    private final LocalSQLLiteDatabase database = LocalSQLLiteDatabase.getInstance();

    @Override
    public boolean isRestrictBetweenProducts(SpecifiedProduct firstSpecifiedProduct, SpecifiedProduct secondSpecifiedProduct) {
        List<SpecifiedRestrict> specifiedRestricts = database.specifiedRestrictDao().getAll();

        for (SpecifiedRestrict specifiedRestrict : specifiedRestricts) {
            if (specifiedRestrict.getFirstSpecificProductId() == firstSpecifiedProduct.getId() && specifiedRestrict.getSecondSpecificProductId() == secondSpecifiedProduct.getId()) {
                return true;
            }

            if (specifiedRestrict.getFirstSpecificProductId() == secondSpecifiedProduct.getId() && specifiedRestrict.getSecondSpecificProductId() == firstSpecifiedProduct.getId()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isRestrictBetweenProducts(int firstSpecifiedProduct, int secondSpecifiedProduct) {
        List<SpecifiedRestrict> specifiedRestricts = database.specifiedRestrictDao().getAll();

        for (SpecifiedRestrict specifiedRestrict : specifiedRestricts) {
            if (specifiedRestrict.getFirstSpecificProductId() == firstSpecifiedProduct && specifiedRestrict.getSecondSpecificProductId() == secondSpecifiedProduct) {
                return true;
            }

            if (specifiedRestrict.getFirstSpecificProductId() == secondSpecifiedProduct && specifiedRestrict.getSecondSpecificProductId() == firstSpecifiedProduct) {
                return true;
            }
        }

        return false;
    }
}
