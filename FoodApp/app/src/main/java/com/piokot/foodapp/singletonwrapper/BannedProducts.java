package com.piokot.foodapp.singletonwrapper;

import com.piokot.foodapp.service.BannedProductsService;
import com.piokot.foodapp.service.impl.BannedProductsServiceImpl;

public class BannedProducts {
    private static final BannedProductsService ourInstance = new BannedProductsServiceImpl();

    public static BannedProductsService getInstance() {
        return ourInstance;
    }
}
